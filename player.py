# player.py

class Player:
    """
    Class representing the game player.
    Attributes:
    - x: X coordinate of the player position.
    - y: Y coordinate of the player position.
    """
    def __init__(self, x, y):
        self.x = x
        self.y = y
    
    def move_left(self):
        """
        Move the player to the left.
        """
        self.x -= 1
    
    def move_right(self):
        """
        Move the player to the right.
        """
        self.x += 1
    
    def move_up(self):
        """
        Move the player up.
        """
        self.y -= 1
    
    def move_down(self):
        """
        Move the player down.
        """
        self.y += 1