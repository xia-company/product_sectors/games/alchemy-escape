# utils.py

import random

def generate_random_number(min, max):
    """
    Generate a random number between min and max (inclusive).
    Args:
    - min: Minimum value.
    - max: Maximum value.
    Returns:
    - Random number between min and max.
    """
    return random.randint(min, max)