# sound.py

import pygame

def load_sound(sound_path):
    """
    Load a sound file.
    Args:
    - sound_path: Path to the sound file.
    Returns:
    - Loaded sound object.
    """
    return pygame.mixer.Sound(sound_path)

def play_sound(sound):
    """
    Play a sound.
    Args:
    - sound: Sound object to play.
    """
    sound.play()