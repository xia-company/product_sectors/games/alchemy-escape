# room.py

class Room:
    """
    Class representing a game room.
    Attributes:
    - name: Name of the room.
    - description: Description of the room.
    - items: List of items in the room.
    """
    def __init__(self, name, description):
        self.name = name
        self.description = description
        self.items = []
    
    def add_item(self, item):
        """
        Add an item to the room.
        Args:
        - item: Item to add.
        """
        self.items.append(item)
    
    def remove_item(self, item):
        """
        Remove an item from the room.
        Args:
        - item: Item to remove.
        """
        self.items.remove(item)