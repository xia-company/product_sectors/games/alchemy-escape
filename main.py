# main.py

from game import Game
from sound import Sound
from utils import Utils

def main():
    """
    Entry point of the game.
    """
    game = Game()
    sound = Sound()
    utils = Utils()

    game.start()

    # Game loop
    while True:
        game.update()
        game.render()
        game.handle_input()
        
        if game.should_end():
            break
    
    game.end()

if __name__ == '__main__':
    main()