```mermaid
classDiagram
    class Game{
        +score: int
        +start(): void
        +end(): void
        +update(): void
        +render(): void
        +handle_input(): void
    }

    class Player{
        +x: int
        +y: int
        +move_left(): void
        +move_right(): void
        +move_up(): void
        +move_down(): void
    }

    class Item{
        +name: str
        +description: str
        +use(): void
    }

    class Room{
        +name: str
        +description: str
        +add_item(item: Item): void
        +remove_item(item: Item): void
    }

    class Ingredient{
        +name: str
        +description: str
    }

    class Potion{
        +ingredients: List[Ingredient]
        +add_ingredient(ingredient: Ingredient): void
        +remove_ingredient(ingredient: Ingredient): void
        +mix_potion(): void
    }

    class Graphics{
        +load_image(image_path: str): Surface
        +draw_text(text: str, font: Font, surface: Surface, x: int, y: int, color: Tuple[int, int, int]): void
        +draw_sprite(spritesheet: Surface, sprite_index: int, surface: Surface, x: int, y: int): void
    }

    class Sound{
        +load_sound(sound_path: str): Sound
        +play_sound(sound: Sound): void
    }

    class Utils{
        +generate_random_number(min: int, max: int) -> int
    }

    class Constants{
        +SCREEN_WIDTH: int
        +SCREEN_HEIGHT: int
    }

    Game "1" -- "1" Player: has
    Game "1" -- "1" Room: has
    Game "1" -- "1" Graphics: has
    Game "1" -- "1" Sound: has
    Game "1" -- "1" Utils: has

    Room "1" -- "n" Item: has
    Potion "1" -- "n" Ingredient: has
```