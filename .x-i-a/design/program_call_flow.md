```mermaid
sequenceDiagram
    participant M as Main
    participant G as Game
    participant P as Player
    participant R as Room
    participant I as Item
    participant S as Sound
    participant U as Utils

    M->>G: G.start()
    G->>+R: Create new room
    G-->>M: Return room
    M->>+P: Create new player
    M->>G: G.update()
    G->>+P: P.move_left()
    G-->>R: R.add_item(I)
    G-->>R: R.remove_item(I)
    M->>G: G.end()
    G->>S: S.play_sound()
    G->>U: U.generate_random_number()
```